import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
}

repositories {
    mavenCentral()
}

group = "com.test.myapp"
version = "0.0.1-SNAPSHOT"

dependencies {
    constraints {
        // Define common dependency versions without including the plugins in the submodule

        // Setting a version if none is specified
        //implementation("something:something:1.2.3")

        // Enforcing a version
        //implementation("something:something") { version { strictly("1.2.3") } }
    }
    // Define common dependencies that are added to all submodules
    // implementation("something:something:1.2.3")
}

// Gradle 8.4 only runs on <= 20. Moving to Java 21 with next Gradle version
val javaJvmTarget = "17"

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs += "-Xjsr305=strict"
        jvmTarget = javaJvmTarget
    }
}

// For Java files
java {
    sourceCompatibility = JavaVersion.toVersion(javaJvmTarget)
}

tasks.withType<Test> {
    useJUnitPlatform()
}
