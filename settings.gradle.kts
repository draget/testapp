rootProject.name = "TestApp"
include(":backend", ":modules:modulea", ":modules:moduleb")

// Define common versions of build plugins here. The plugins still need to be applied in each module
pluginManagement {
    plugins {
        id("testapp.kotlin-common-conventions") version "1.0.0"
        id("org.springframework.boot") version "3.1.4"
        id("io.spring.dependency-management") version "1.1.3"
        id("org.jetbrains.kotlin.jvm") version "1.8.22"
    }
}
