// The module's build plugins. If you want to use common versions, define them in the root settings.gradle.kts
plugins {
    id("testapp.kotlin-common-conventions")

    id("org.jetbrains.kotlin.jvm")
    id("org.springframework.boot")
    id("io.spring.dependency-management")
}

// The dependencies. If you want common versions, define them in the buildSrc plugin constraints
dependencies {
    implementation(project(":modules:modulea"))
    implementation(project(":modules:moduleb"))

    implementation("org.springframework.boot:spring-boot-starter")
}
