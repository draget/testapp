package com.test.myapp;

import com.test.myapp.modulea.FoobarA
import com.test.myapp.moduleb.FoobarB
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication


@SpringBootApplication
open class BackendApplication : CommandLineRunner {
    override fun run(vararg args: String?) {
        FoobarA.someMethod()
        FoobarB.someMethod()
        println("Hello from backend!")
    }
}

fun main(args: Array<String>) {
    runApplication<BackendApplication>(*args)
}
