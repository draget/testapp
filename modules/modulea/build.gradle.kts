// The module's build plugins. If you want to use common versions, define them in the root settings.gradle.kts
plugins {
    id("testapp.kotlin-common-conventions")
}

// The dependencies. If you want common versions, define them in the buildSrc plugin constraints
dependencies {
//    implementation("org.springframework.boot:spring-boot-starter-security")
//    implementation("org.springframework.boot:spring-boot-starter-validation")
//    // ...
}
